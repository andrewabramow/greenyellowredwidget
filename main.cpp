#include <QApplication>
#include <QPushButton>
#include <QPixmap>
#include <QPainter>
#include <QPaintEvent>
#include <QSlider>
#include <QVBoxLayout>
#include <QSpinBox>
#include <iostream>

class ImageButton : public QPushButton
{
    Q_OBJECT
public:
    ImageButton() = default;
    ImageButton(QWidget* parent);
    void paintEvent(QPaintEvent *e) override;
    QSize minimumSizeHint() const override;

public slots:
    void setGreen();
    void setYellow();
    void setRed();
private:
    QPixmap mCurrentButtonPixmap;
    QPixmap mButtonGreenPixmap;
    QPixmap mButtonYellowPixmap;
    QPixmap mButtonRedPixmap;
};

ImageButton::ImageButton(QWidget *parent)
{
    setParent(parent);
    setToolTip("Stop");
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    mButtonGreenPixmap = QPixmap("D:/QtProjects/untitled/Green.png");
    mButtonYellowPixmap = QPixmap("D:/QtProjects/untitled/Yellow.png");
    mButtonRedPixmap = QPixmap("D:/QtProjects/untitled/Red.png");
    mCurrentButtonPixmap = mButtonGreenPixmap;
    setGeometry(mCurrentButtonPixmap.rect());
}

void ImageButton::paintEvent(QPaintEvent *e)
{
    QPainter p(this);
    p.drawPixmap(e->rect(), mCurrentButtonPixmap);
}

QSize ImageButton::minimumSizeHint() const
{
    return QSize(100,100);
}

void ImageButton::setGreen()
{
    mCurrentButtonPixmap = mButtonGreenPixmap;
    update();
}

void ImageButton::setYellow()
{
    mCurrentButtonPixmap = mButtonYellowPixmap;
    update();
}

void ImageButton::setRed()
{
    mCurrentButtonPixmap = mButtonRedPixmap;
    update();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget* window = new QWidget;
    auto *layout = new QVBoxLayout(window);

    auto *spinbox = new QSpinBox();
    spinbox -> setMaximum(0);
    spinbox -> setMaximum(100);

    auto slider = new QSlider(Qt::Horizontal);
    slider -> setMaximum(0);
    slider -> setMaximum(100);
    slider->resize(50,10);

    ImageButton *circle = new ImageButton(nullptr);
    circle->setFixedSize(200,250);

    layout->addWidget(circle);
    layout->addWidget(slider);
    layout->addWidget(spinbox);

    QObject::connect(slider, &QSlider::valueChanged, [slider, &circle](int newValue)
    { if (newValue>0 && newValue<33) circle->setGreen();
      else if (newValue>33 && newValue<66) circle->setYellow();
      else if (newValue>66) circle->setRed();});

    QObject::connect(slider, &QSlider::valueChanged, spinbox, &QSpinBox::setValue);
        QObject::connect(spinbox, QOverload<int>::of(&QSpinBox::valueChanged), slider, &QSlider::setValue);

    window->show();

    return a.exec();
}

#include <main.moc>
